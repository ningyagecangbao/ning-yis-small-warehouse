%% GA优化BP神经网络时序预测
tic;
clear;                  
clc;                  
result = xlsread('2017测试数据集部分.xlsx');
addpath('goat\')
num_samples = length(result);  % 样本个数 
kim = 15;                      % 延时步长（kim个历史数据作为自变量）
zim =  1;                      % 跨zim个时间点进行预测

for i = 1: num_samples - kim - zim + 1
    res(i, :) = [reshape(result(i: i + kim - 1), 1, kim), result(i + kim + zim - 1)];
end

temp = 1: 1: 8761;

P_train = res(temp(1: 7000), 1: 15)';
T_train = res(temp(1: 7000), 16)';
M = size(P_train, 2);

P_test = res(temp(7001: 8745), 1: 15)';
T_test = res(temp(7001: 8745), 16)';
N = size(P_test, 2);

[p_train, ps_input] = mapminmax(P_train, 0, 1);
p_test = mapminmax('apply', P_test, ps_input);

[t_train, ps_output] = mapminmax(T_train, 0, 1);
t_test = mapminmax('apply', T_test, ps_output);

S1 = 5;                          
net = newff(p_train, t_train, S1);

net.trainParam.epochs = 1000;         
net.trainParam.goal   = 1e-6;        
net.trainParam.lr     = 0.01;        

gen = 50;                      
pop_num = 5;                   
S = size(p_train, 1) * S1 + S1 * size(t_train, 1) + S1 + size(t_train, 1);
                               
bounds = ones(S, 1) * [-1, 1];  

prec = [1e-6, 1];              
normGeomSelect = 0.09;          
arithXover = 2;                 
nonUnifMutation = [2 gen 3];    
initPpp = initializega(pop_num, bounds, 'gabpEval', [], prec);  

[Bestpop, endPop, bPop, trace] = ga(bounds, 'gabpEval', [], initPpp, [prec, 0], 'maxGenTerm', gen,...
                           'normGeomSelect', normGeomSelect, 'arithXover', arithXover, ...
                           'nonUnifMutation', nonUnifMutation);

[val, W1, B1, W2, B2] = gadecod(Bestpop);

net.IW{1, 1} = W1;
net.LW{2, 1} = W2;
net.b{1}     = B1;
net.b{2}     = B2;

net.trainParam.showWindow = 1;       
net = train(net, p_train, t_train);  

t_sim1 = sim(net, p_train);
t_sim2 = sim(net, p_test );

T_sim1 = mapminmax('reverse', t_sim1, ps_output);
T_sim2 = mapminmax('reverse', t_sim2, ps_output);

error1 = sqrt(sum((T_sim1 - T_train).^2) ./ M);
error2 = sqrt(sum((T_sim2 - T_test ).^2) ./ N);

R1 = 1 - norm(T_train - T_sim1)^2 / norm(T_train - mean(T_train))^2;
R2 = 1 - norm(T_test -  T_sim2)^2 / norm(T_test -  mean(T_test ))^2;

disp(['训练集R2：', num2str(R1)])
disp(['测试集R2：', num2str(R2)])

mae1 = sum(abs(T_sim1 - T_train)) ./ M ;
mae2 = sum(abs(T_sim2 - T_test )) ./ N ;

disp(['训练集MAE：', num2str(mae1)])
disp(['测试集MAE：', num2str(mae2)])

mbe1 = sum(T_sim1 - T_train) ./ M ;
mbe2 = sum(T_sim2 - T_test ) ./ N ;

disp(['训练集MBE：', num2str(mbe1)])
disp(['测试集MBE：', num2str(mbe2)])

mape1 = mean(abs((T_sim1 - T_train) ./ T_train)) * 100;
mape2 = mean(abs((T_sim2 - T_test) ./ T_test)) * 100;

disp(['训练集MAPE：', num2str(mape1)])
disp(['测试集MAPE：', num2str(mape2)])

figure
plot(trace(:, 1), 1 ./ trace(:, 2), 'LineWidth', 1);
xlabel('迭代次数');
ylabel('适应度值');
string = {'适应度变化曲线'};
title(string)
grid on

figure
plot(1: M, T_train, 'r-', 1: M, T_sim1, 'b--', 'LineWidth', 1)
legend('真实值', '预测值')
xlabel('预测样本')
ylabel('预测结果')
string = {'训练集预测结果对比'; ['RMSE=' num2str(error1)]; ['R2=' num2str(R1)]};
title(string)
xlim([1, M])
grid

figure
plot(1: N, T_test, 'r-', 1: N, T_sim2, 'b--', 'LineWidth', 1)
legend('真实值', '预测值')
xlabel('预测样本')
ylabel('预测结果')
string = {'测试集预测结果对比'; ['RMSE=' num2str(error2)]; ['R2=' num2str(R2)]};
title(string)
xlim([1, N])
grid

figure
scatter(T_train, T_sim1, 20, 'm')
hold on
plot(xlim, ylim, '--r')
xlabel('训练集真实值');
ylabel('训练集预测值');
xlim([min(T_train) max(T_train)])
ylim([min(T_sim1) max(T_sim1)])
title('训练集预测值&真实值')

figure
scatter(T_test, T_sim2, 20, 'b')
hold on
plot(xlim, ylim, '--r')
xlabel('测试集真实值');
ylabel('测试集预测值');
xlim([min(T_test) max(T_test)])
ylim([min(T_sim2) max(T_sim2)])
title('测试集预测值&真实值')
toc
